# Lineage-17.1 microG Installation

An Installation Guide for installing microG on devices running LineageOS 17.1. See steps below.

1. **Download tings:**
    - [NanoDroid Beta](https://downloads.nanolx.org/NanoDroid/Beta/)
        - Download file named NanoDroid-microG-[version].zip
    - [Patching script](https://del.dog/hulycobanu.sh)
        - Copy-paste into `script.sh` file
        - `chmod +x` the file
    - [TWRP](https://twrp.me/Devices/)
        1. Find manufacturer   
        2. Find device codename
        3. Download the latest `.img` file
2. **Do tings with the tings:**
    1. Reboot into bootloader 
        1. Reboot the device using the weird button config ***OR***
        2. Run `adb reboot-bootloader` in a terminal
    2. Flash TWRP temporarily to the device
        - Run `fastboot boot twrp-[version]-[device codename].img` in a terminal
    3. Mount the `/system` partition
        1. Tap *Mount*
        2. Tap the checkbox next to `System`
    4. Run the patching script
        1. `./script.sh` (or whatever you named it)  
        2. Wait for it to finish before proceeding
    5. Enable ADB Sideload and install microG
        1. Go back to the home screen for TWRP, click on *Advanced*, then *ADB Sideload*, then swipe the slider to the right
        2. Run `adb sideload NanoDroid-microG-[version].zip`
    6. Reboot the device
        1. Reboot as if you were turning it on as normal
    7. Cum (Finished)
        - You finished. Now go stroke your FOSS boner in private.